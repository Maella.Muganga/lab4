package datastructure;

import cellular.CellState;
public class CellGrid implements IGrid {
    int rows;
    int columns;
    CellState initialState;
    CellState[][] DimTwo;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;
        this.initialState = initialState;
        this.DimTwo = new CellState[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                DimTwo[i][j] = initialState;
            }
        }
    }

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        DimTwo[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        return DimTwo[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid GridCopy = new CellGrid(rows, columns, CellState.DEAD);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                GridCopy.set(i, j, this.get(i, j));
            }
        }
        return GridCopy;
    }
}
